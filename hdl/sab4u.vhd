--
-- Copyright (C) Telecom ParisTech
-- Copyright (C) Renaud Pacalet (renaud.pacalet@telecom-paristech.fr)
-- 
-- This file must be used under the terms of the CeCILL. This source
-- file is licensed as described in the file COPYING, which you should
-- have received as part of this distribution. The terms are also
-- available at:
-- http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sab4u is
  generic(
    frequency:       positive := 200; -- MHz
    debouncer_delay: positive := 10;  -- ms
    blink_delay:     positive := 10   -- ms
  );
  port(
    aclk:       in  std_ulogic;  -- clock
    aresetn:    in  std_ulogic;  -- synchronous, active low, reset
    btn:        in  std_ulogic;  -- command button
    sw:         in  std_ulogic_vector(7 downto 0); -- slide switches
    led:        out std_ulogic_vector(7 downto 0); -- leds

    --------------------------------
    -- axi lite slave port s0_axi
    -- 12 bits addresses, that is 4kB only, enough for the interface registers
    -- Addresses are mapped in [0x00_a000_0000, 0x00_a000_1000[ by the scripts/vvsyn.tcl synthesis script
    --------------------------------
    -- inputs (master to slave)
    ------------------------------
    -- read address channel
    s0_axi_araddr:  in  std_ulogic_vector(11 downto 0);
    s0_axi_arprot:  in  std_ulogic_vector(2 downto 0);
    s0_axi_arvalid: in  std_ulogic;
    -- read data channel
    s0_axi_rready:  in  std_ulogic;
    -- write address channel
    s0_axi_awaddr:  in  std_ulogic_vector(11 downto 0);
    s0_axi_awprot:  in  std_ulogic_vector(2 downto 0);
    s0_axi_awvalid: in  std_ulogic;
    -- write data channel
    s0_axi_wdata:   in  std_ulogic_vector(63 downto 0);
    s0_axi_wstrb:   in  std_ulogic_vector(7 downto 0);
    s0_axi_wvalid:  in  std_ulogic;
    -- write response channel
    s0_axi_bready:  in  std_ulogic;
    -------------------------------
    -- outputs (slave to master) --
    -------------------------------
    -- read address channel
    s0_axi_arready: out std_ulogic;
    -- read data channel
    s0_axi_rdata:   out std_ulogic_vector(63 downto 0);
    s0_axi_rresp:   out std_ulogic_vector(1 downto 0);
    s0_axi_rvalid:  out std_ulogic;
    -- write address channel
    s0_axi_awready: out std_ulogic;
    -- write data channel
    s0_axi_wready:  out std_ulogic;
    -- write response channel
    s0_axi_bresp:   out std_ulogic_vector(1 downto 0);
    s0_axi_bvalid:  out std_ulogic;

    ---------------------------
    -- axi slave port s1_axi
    -- 32 bits addresses, that is 4GB, enough for the 4GB DDR of the board
    -- Addresses are mapped to [0x05_0000_0000, 0x06_0000_0000[ by the scripts/vvsyn.tcl synthesis script
    ------------------------------
    -- inputs (master to slave) --
    ------------------------------
    -- read address channel
    s1_axi_arid:    in  std_ulogic_vector(15 downto 0);
    s1_axi_araddr:  in  std_ulogic_vector(31 downto 0);
    s1_axi_arlen:   in  std_ulogic_vector(7 downto 0);
    s1_axi_arsize:  in  std_ulogic_vector(2 downto 0);
    s1_axi_arburst: in  std_ulogic_vector(1 downto 0);
    s1_axi_arlock:  in  std_ulogic;
    s1_axi_arcache: in  std_ulogic_vector(3 downto 0);
    s1_axi_arprot:  in  std_ulogic_vector(2 downto 0);
    s1_axi_arqos:   in  std_ulogic_vector(3 downto 0);
    s1_axi_aruser:  in  std_ulogic_vector(15 downto 0);
    s1_axi_arvalid: in  std_ulogic;
    -- read data channel
    s1_axi_rready:  in  std_ulogic;
    -- write address channel
    s1_axi_awid:    in  std_ulogic_vector(15 downto 0);
    s1_axi_awaddr:  in  std_ulogic_vector(31 downto 0);
    s1_axi_awlen:   in  std_ulogic_vector(7 downto 0);
    s1_axi_awsize:  in  std_ulogic_vector(2 downto 0);
    s1_axi_awburst: in  std_ulogic_vector(1 downto 0);
    s1_axi_awlock:  in  std_ulogic;
    s1_axi_awcache: in  std_ulogic_vector(3 downto 0);
    s1_axi_awprot:  in  std_ulogic_vector(2 downto 0);
    s1_axi_awqos:   in  std_ulogic_vector(3 downto 0);
    s1_axi_awuser:  in  std_ulogic_vector(15 downto 0);
    s1_axi_awvalid: in  std_ulogic;
    -- write data channel
    s1_axi_wdata:   in  std_ulogic_vector(63 downto 0);
    s1_axi_wstrb:   in  std_ulogic_vector(7 downto 0);
    s1_axi_wlast:   in  std_ulogic;
    s1_axi_wvalid:  in  std_ulogic;
    -- write response channel
    s1_axi_bready:  in  std_ulogic;
    -------------------------------
    -- outputs (slave to master) --
    -------------------------------
    -- read address channel
    s1_axi_arready: out std_ulogic;
    -- read data channel
    s1_axi_rid:     out std_ulogic_vector(15 downto 0);
    s1_axi_rdata:   out std_ulogic_vector(63 downto 0);
    s1_axi_rresp:   out std_ulogic_vector(1 downto 0);
    s1_axi_rlast:   out std_ulogic;
    s1_axi_rvalid:  out std_ulogic;
    -- write address channel
    s1_axi_awready: out std_ulogic;
    -- write data channel
    s1_axi_wready:  out std_ulogic;
    -- write response channel
    s1_axi_bid:     out std_ulogic_vector(15 downto 0);
    s1_axi_bresp:   out std_ulogic_vector(1 downto 0);
    s1_axi_bvalid:  out std_ulogic;

    ---------------------------
    -- axi master port m_axi --
    -- 36 bits addresses, that is 64GB, needed to access the [0x08_0000_0000, 0x09_0000_0000[ range
    -- Addresses are automatically mapped to [0x00_0000_0000, 0x10_0000_0000[ by Vivado
    -- but only the [0x08_0000_0000, 0x09_0000_0000[ range is used (see the s1_axi to m_axi translation below)
    ---------------------------
    -------------------------------
    -- outputs (slave to master) --
    -------------------------------
    -- read address channel
    m_axi_arid:    out std_ulogic_vector(5 downto 0);
    m_axi_araddr:  out std_ulogic_vector(35 downto 0);
    m_axi_arlen:   out std_ulogic_vector(7 downto 0);
    m_axi_arsize:  out std_ulogic_vector(2 downto 0);
    m_axi_arburst: out std_ulogic_vector(1 downto 0);
    m_axi_arlock:  out std_ulogic;
    m_axi_arcache: out std_ulogic_vector(3 downto 0);
    m_axi_arprot:  out std_ulogic_vector(2 downto 0);
    m_axi_arqos:   out std_ulogic_vector(3 downto 0);
    m_axi_aruser:  out std_ulogic;
    m_axi_arvalid: out std_ulogic;
    -- read data channel
    m_axi_rready:  out std_ulogic;
    -- write address channel
    m_axi_awid:    out std_ulogic_vector(5 downto 0);
    m_axi_awaddr:  out std_ulogic_vector(35 downto 0);
    m_axi_awlen:   out std_ulogic_vector(7 downto 0);
    m_axi_awsize:  out std_ulogic_vector(2 downto 0);
    m_axi_awburst: out std_ulogic_vector(1 downto 0);
    m_axi_awlock:  out std_ulogic;
    m_axi_awcache: out std_ulogic_vector(3 downto 0);
    m_axi_awprot:  out std_ulogic_vector(2 downto 0);
    m_axi_awqos:   out std_ulogic_vector(3 downto 0);
    m_axi_awuser:  out std_ulogic;
    m_axi_awvalid: out std_ulogic;
    -- write data channel
    m_axi_wdata:   out std_ulogic_vector(63 downto 0);
    m_axi_wstrb:   out std_ulogic_vector(7 downto 0);
    m_axi_wlast:   out std_ulogic;
    m_axi_wvalid:  out std_ulogic;
    -- write response channel
    m_axi_bready:  out std_ulogic;
    ------------------------------
    -- inputs (slave to master) --
    ------------------------------
    -- read address channel
    m_axi_arready: in  std_ulogic;
    -- read data channel
    m_axi_rid:     in  std_ulogic_vector(5 downto 0);
    m_axi_rdata:   in  std_ulogic_vector(63 downto 0);
    m_axi_rresp:   in  std_ulogic_vector(1 downto 0);
    m_axi_rlast:   in  std_ulogic;
    m_axi_rvalid:  in  std_ulogic;
    -- write address channel
    m_axi_awready: in  std_ulogic;
    -- write data channel
    m_axi_wready:  in  std_ulogic;
    -- write response channel
    m_axi_bid:     in  std_ulogic_vector(5 downto 0);
    m_axi_bresp:   in  std_ulogic_vector(1 downto 0);
    m_axi_bvalid:  in  std_ulogic
  );
end entity sab4u;

architecture rtl of sab4u is

  constant axi_resp_okay:   std_ulogic_vector(1 downto 0) := "00";
  constant axi_resp_exokay: std_ulogic_vector(1 downto 0) := "01";
  constant axi_resp_slverr: std_ulogic_vector(1 downto 0) := "10";
  constant axi_resp_decerr: std_ulogic_vector(1 downto 0) := "11";

  -- registers
  constant ndwords: positive := 1 + 1 + 4; -- status, r, axi_reg
  signal regs: std_ulogic_vector(64 * ndwords - 1 downto 0);

  -- status register
  alias status:  std_ulogic_vector(63 downto 0) is regs(63 downto 0);

  alias err:   std_ulogic_vector(7 downto 0) is status(7 downto 0);
  alias life:  std_ulogic_vector(7 downto 0) is status(15 downto 8);
  alias cnt:   std_ulogic_vector(7 downto 0) is status(23 downto 16);
  alias arcnt: std_ulogic_vector(7 downto 0) is status(31 downto 24);
  alias rcnt:  std_ulogic_vector(7 downto 0) is status(39 downto 32);
  alias awcnt: std_ulogic_vector(7 downto 0) is status(47 downto 40);
  alias wcnt:  std_ulogic_vector(7 downto 0) is status(55 downto 48);
  alias bcnt:  std_ulogic_vector(7 downto 0) is status(63 downto 56);

  signal ucnt:     unsigned(7 downto 0);
  signal uarcnt:   unsigned(7 downto 0);
  signal urcnt:    unsigned(7 downto 0);
  signal uawcnt:   unsigned(7 downto 0);
  signal uwcnt:    unsigned(7 downto 0);
  signal ubcnt:    unsigned(7 downto 0);
  signal uslsw:    unsigned(7 downto 0);

  -- r register
  alias r: std_ulogic_vector(63 downto 0) is regs(127 downto 64);

  -- axi_reg register
  alias axi_reg: std_ulogic_vector(64 * (ndwords - 2) - 1 downto 0) is regs(64 * ndwords - 1 downto 128);
  alias axi_reg_arid:    std_ulogic_vector(15 downto 0) is axi_reg(31 * 8 + 7 downto 30 * 8);     -- bytes 30-31
  alias axi_reg_awid:    std_ulogic_vector(15 downto 0) is axi_reg(29 * 8 + 7 downto 28 * 8);     -- bytes 28-29
  alias axi_reg_araddr:  std_ulogic_vector(31 downto 0) is axi_reg(27 * 8 + 7 downto 24 * 8);     -- bytes 24-27
  alias axi_reg_awaddr:  std_ulogic_vector(31 downto 0) is axi_reg(23 * 8 + 7 downto 20 * 8);     -- bytes 20-23
  alias axi_reg_arlen:   std_ulogic_vector(7 downto 0)  is axi_reg(19 * 8 + 7 downto 19 * 8);     -- byte  19
  alias axi_reg_awlen:   std_ulogic_vector(7 downto 0)  is axi_reg(18 * 8 + 7 downto 18 * 8);     -- byte  18
  alias axi_reg_arsize:  std_ulogic_vector(2 downto 0)  is axi_reg(17 * 8 + 6 downto 17 * 8 + 4); -- byte  17, bits 6..4
  alias axi_reg_awsize:  std_ulogic_vector(2 downto 0)  is axi_reg(17 * 8 + 2 downto 17 * 8);     -- byte  17, bits 2..0
  alias axi_reg_arburst: std_ulogic_vector(1 downto 0)  is axi_reg(16 * 8 + 7 downto 16 * 8 + 6); -- byte  16, bits 7..6
  alias axi_reg_awburst: std_ulogic_vector(1 downto 0)  is axi_reg(16 * 8 + 5 downto 16 * 8 + 4); -- byte  16, bits 5..4
  alias axi_reg_arvalid: std_ulogic                     is axi_reg(16 * 8 + 3);                   -- byte  16, bit  3
  alias axi_reg_awvalid: std_ulogic                     is axi_reg(16 * 8 + 2);                   -- byte  16, bit  2
  alias axi_reg_wlast:   std_ulogic                     is axi_reg(16 * 8 + 1);                   -- byte  16, bit  1
  alias axi_reg_wvalid:  std_ulogic                     is axi_reg(16 * 8);                       -- byte  16, bit  0
  alias axi_reg_arlock:  std_ulogic                     is axi_reg(15 * 8 + 7);                   -- byte  15, bit  7
  alias axi_reg_awlock:  std_ulogic                     is axi_reg(15 * 8 + 6);                   -- byte  15, bit  6
  alias axi_reg_arprot:  std_ulogic_vector(2 downto 0)  is axi_reg(15 * 8 + 5 downto 15 * 8 + 3); -- byte  15, bits 5..3
  alias axi_reg_awprot:  std_ulogic_vector(2 downto 0)  is axi_reg(15 * 8 + 2 downto 15 * 8);     -- byte  15, bits 2..0
  alias axi_reg_arcache: std_ulogic_vector(3 downto 0)  is axi_reg(14 * 8 + 7 downto 14 * 8 + 4); -- byte  14, bits 7..4
  alias axi_reg_awcache: std_ulogic_vector(3 downto 0)  is axi_reg(14 * 8 + 3 downto 14 * 8);     -- byte  14, bits 3..0
  alias axi_reg_arqos:   std_ulogic_vector(3 downto 0)  is axi_reg(13 * 8 + 7 downto 13 * 8 + 4); -- byte  13, bits 7..4
  alias axi_reg_awqos:   std_ulogic_vector(3 downto 0)  is axi_reg(13 * 8 + 3 downto 13 * 8);     -- byte  13, bits 3..0
  alias axi_reg_aruser:  std_ulogic_vector(15 downto 0) is axi_reg(12 * 8 + 7 downto 11 * 8);     -- bytes 11-12
  alias axi_reg_awuser:  std_ulogic_vector(15 downto 0) is axi_reg(10 * 8 + 7 downto 9 * 8);      -- bytes 9-10
  alias axi_reg_wdata:   std_ulogic_vector(63 downto 0) is axi_reg(8 * 8 + 7 downto 8);           -- bytes 1-8
  alias axi_reg_wstrb:   std_ulogic_vector(7 downto 0)  is axi_reg(7 downto 0);                   -- byte  0

  -- 10 msbs of s1_axi_axid, lock flags
  signal s1_axi_m2s_arid: std_ulogic_vector(15 downto 6);
  signal s1_axi_m2s_arid_lock: std_ulogic;
  signal s1_axi_m2s_awid: std_ulogic_vector(15 downto 6);
  signal s1_axi_m2s_awid_lock: std_ulogic;

  -- local copies of outputs
  signal s1_axi_arready_l: std_ulogic;
  signal s1_axi_rvalid_l: std_ulogic;
  signal s1_axi_awready_l: std_ulogic;
  signal s1_axi_wready_l: std_ulogic;
  signal s1_axi_bvalid_l: std_ulogic;

  -- debouncer
  signal btn_sd: std_ulogic; -- synchronized push-button
  signal btn_re: std_ulogic; -- rising edge of synchronized and debounced push-button

  -- and reduction of std_ulogic_vector
  function and_reduce(v: std_ulogic_vector) return std_ulogic is
    variable tmp: std_ulogic_vector(v'length - 1 downto 0) := v;
  begin
    if tmp'length = 0 then
      return '0';
    elsif tmp'length = 1 then
      return tmp(0);
    else
      return and_reduce(tmp(tmp'length - 1 downto tmp'length / 2)) and
             and_reduce(tmp(tmp'length / 2 - 1 downto 0));
    end if;
  end function and_reduce;

begin

  cnt   <= std_ulogic_vector(ucnt);
  arcnt <= std_ulogic_vector(uarcnt);
  rcnt  <= std_ulogic_vector(urcnt);
  awcnt <= std_ulogic_vector(uawcnt);
  wcnt  <= std_ulogic_vector(uwcnt);
  bcnt  <= std_ulogic_vector(ubcnt);

  -- synchronizer - debouncer
  process(aclk)
    constant deb_cnt_max:  positive := frequency * debouncer_delay * 1000;
    variable deb_cnt:      natural range 0 to deb_cnt_max - 1;
    variable deb_sync_in:  std_ulogic_vector(0 to 1);
    variable deb_sync_out: std_ulogic_vector(0 to 1);
  begin
    if rising_edge(aclk) then
      if aresetn = '0' then
        btn_re       <= '0';
        deb_cnt      := deb_cnt_max - 1;
        deb_sync_in  := (others => '0');
        deb_sync_out := (others => '0');
      else
        btn_re <= deb_sync_out(0) and (not deb_sync_out(1));
        if deb_sync_in(1) = '0' then
          deb_cnt      := deb_cnt_max - 1;
          deb_sync_out := '0' & deb_sync_out(0);
        else
          if deb_cnt = 0 then
            deb_sync_out := '1' & deb_sync_out(0);
          else
            deb_cnt      := deb_cnt - 1;
            deb_sync_out := '0' & deb_sync_out(0);
          end if;
        end if;
        deb_sync_in := btn & deb_sync_in(0);
        btn_sd      <= deb_sync_in(1);
      end if;
    end if;
  end process;

  -- led outputs
  process(regs, uslsw, btn_sd)
    variable idx: natural range 0 to 63;
  begin
    idx := to_integer(uslsw(5 downto 0));
    if btn_sd = '1' then
      led <= std_ulogic_vector(uslsw);
    elsif idx < ndwords * 8 then
      led <= std_ulogic_vector(regs(8 * idx + 7 downto 8 * idx));
    else
      led <= (others => '0');
    end if;
  end process;

  -- s1_axi_axid register
  process(aclk)
  begin
    if rising_edge(aclk) then
      if aresetn = '0' then
        s1_axi_m2s_arid <= (others => '0');
        s1_axi_m2s_arid_lock <= '0';
        s1_axi_m2s_awid <= (others => '0');
        s1_axi_m2s_awid_lock <= '0';
        err <= (others => '0');
      else
        if s1_axi_arvalid = '1' then
          if s1_axi_m2s_arid_lock = '0' then
            s1_axi_m2s_arid_lock <= '1';
            s1_axi_m2s_arid <= s1_axi_arid(15 downto 6);
          elsif s1_axi_m2s_arid /= s1_axi_arid(15 downto 6) then
            err(0) <= '1';
          end if;
        end if;
        if s1_axi_awvalid = '1' then
          if s1_axi_m2s_awid_lock = '0' then
            s1_axi_m2s_awid_lock <= '1';
            s1_axi_m2s_awid <= s1_axi_awid(15 downto 6);
          elsif s1_axi_m2s_awid /= s1_axi_awid(15 downto 6) then
            err(1) <= '1';
          end if;
        end if;
      end if;
    end if;
  end process;

  -- status register
  process(aclk)
    constant life_cnt_max: positive := frequency * blink_delay * 1000;
    variable life_cnt: natural range 0 to life_cnt_max - 1; -- life monitor counter
    variable life_left_to_right: boolean;
  begin
    if rising_edge(aclk) then
      if aresetn = '0' then
        life   <= x"01";
        ucnt   <= x"00";
        uarcnt <= x"00";
        urcnt  <= x"00";
        uawcnt <= x"00";
        uwcnt  <= x"00";
        ubcnt  <= x"00";
        uslsw  <= x"00";
        life_cnt := life_cnt_max - 1;
        life_left_to_right := true;
      else
        -- life monitor
        if life_cnt = 0 then
          if life_left_to_right then
            life <= life(0) & life(7 downto 1);
            if life(1) = '1' then
              life_left_to_right := not life_left_to_right;
            end if;
          else
            life <= life(6 downto 0) & life(7);
            if life(6) = '1' then
              life_left_to_right := not life_left_to_right;
            end if;
          end if;
          life_cnt := life_cnt_max - 1;
        else
          life_cnt := life_cnt - 1;
        end if;
        -- btn event counter
        if btn_re = '1' then
          if ucnt = 255 then
            ucnt <= (others => '0');
          else
            ucnt <= ucnt + 1;
          end if;
        end if;
        -- s1_axi address read transactions counter
        if s1_axi_arvalid = '1' and s1_axi_arready_l = '1' then
          uarcnt <= uarcnt + 1;
        end if;
        -- s1_axi data read transactions counter
        if s1_axi_rvalid_l = '1' and s1_axi_rready = '1' then
          urcnt <= urcnt + 1;
        end if;
        -- s1_axi address write transactions counter
        if s1_axi_awvalid = '1' and s1_axi_awready_l = '1' then
          uawcnt <= uawcnt + 1;
        end if;
        -- s1_axi data write transactions counter
        if s1_axi_wvalid = '1' and s1_axi_wready_l = '1' then
          uwcnt <= uwcnt + 1;
        end if;
        -- s1_axi write response transactions counter
        if s1_axi_bvalid_l = '1' and s1_axi_bready = '1' then
          ubcnt <= ubcnt + 1;
        end if;
        -- slide switches
        uslsw <= unsigned(sw);
      end if;
    end if;
  end process;

  -- axi_reg register
  process(aclk)
  begin
    if rising_edge(aclk) then
      if aresetn = '0' then
        axi_reg <= (others => '0');
      else
        if s1_axi_arvalid = '1' then
          axi_reg_arid    <= s1_axi_arid;
          axi_reg_araddr  <= s1_axi_araddr;
          axi_reg_arlen   <= s1_axi_arlen;
          axi_reg_arsize  <= s1_axi_arsize;
          axi_reg_arburst <= s1_axi_arburst;
          axi_reg_arvalid <= s1_axi_arvalid;
          axi_reg_arlock  <= s1_axi_arlock;
          axi_reg_arprot  <= s1_axi_arprot;
          axi_reg_arcache <= s1_axi_arcache;
          axi_reg_arqos   <= s1_axi_arqos;
          axi_reg_aruser  <= s1_axi_aruser;
        end if;
        if s1_axi_awvalid = '1' then
          axi_reg_awid    <= s1_axi_awid;
          axi_reg_awaddr  <= s1_axi_awaddr;
          axi_reg_awlen   <= s1_axi_awlen;
          axi_reg_awsize  <= s1_axi_awsize;
          axi_reg_awburst <= s1_axi_awburst;
          axi_reg_awvalid <= s1_axi_awvalid;
          axi_reg_awlock  <= s1_axi_awlock;
          axi_reg_awprot  <= s1_axi_awprot;
          axi_reg_awcache <= s1_axi_awcache;
          axi_reg_awqos   <= s1_axi_awqos;
          axi_reg_awuser  <= s1_axi_awuser;
        end if;
        if s1_axi_wvalid = '1' then
          axi_reg_wlast <= s1_axi_wlast;
          axi_reg_wvalid <= s1_axi_wvalid;
          axi_reg_wdata <= s1_axi_wdata;
          axi_reg_wstrb <= s1_axi_wstrb;
        end if;
      end if;
    end if;
  end process;

  -- Forwarding of s1_axi read-write requests to m_axi and of m_axi responses to s1_axi
  -- s1_axi to m_axi addresses translations are simply 32 to 36 bits extension with the four Most Significant Bits (MSBs) hard wired to "1000" binary (0x8
  -- hexadecimal)
  m_axi_arid    <= s1_axi_arid(5 downto 0);
  m_axi_araddr  <= "1000" & s1_axi_araddr;
  m_axi_arlen   <= s1_axi_arlen;
  m_axi_arsize  <= s1_axi_arsize;
  m_axi_arburst <= s1_axi_arburst;
  m_axi_arlock  <= s1_axi_arlock;
  m_axi_arcache <= s1_axi_arcache;
  m_axi_arprot  <= s1_axi_arprot;
  m_axi_arqos   <= s1_axi_arqos;
  m_axi_aruser  <= '0';
  m_axi_arvalid <= s1_axi_arvalid when uslsw(7) = '0' else '0';
  m_axi_rready  <= s1_axi_rready when uslsw(7) = '0' else '0';
  m_axi_awid    <= s1_axi_awid(5 downto 0);
  m_axi_awaddr  <= "1000" & s1_axi_awaddr;
  m_axi_awlen   <= s1_axi_awlen;
  m_axi_awsize  <= s1_axi_awsize;
  m_axi_awburst <= s1_axi_awburst;
  m_axi_awlock  <= s1_axi_awlock;
  m_axi_awcache <= s1_axi_awcache;
  m_axi_awprot  <= s1_axi_awprot;
  m_axi_awqos   <= s1_axi_awqos;
  m_axi_awuser  <= '0';
  m_axi_awvalid <= s1_axi_awvalid when uslsw(7) = '0' else '0';
  m_axi_wdata   <= s1_axi_wdata;
  m_axi_wstrb   <= s1_axi_wstrb;
  m_axi_wlast   <= s1_axi_wlast;
  m_axi_wvalid  <= s1_axi_wvalid when uslsw(7) = '0' else '0';
  m_axi_bready  <= s1_axi_bready when uslsw(7) = '0' else '0';

  s1_axi_arready_l <= m_axi_arready when uslsw(7) = '0' else '0';
  s1_axi_rid       <= s1_axi_m2s_arid & m_axi_rid;
  s1_axi_rdata     <= m_axi_rdata;
  s1_axi_rresp     <= m_axi_rresp;
  s1_axi_rlast     <= m_axi_rlast;
  s1_axi_rvalid_l  <= m_axi_rvalid when uslsw(7) = '0' else '0';
  s1_axi_awready_l <= m_axi_awready when uslsw(7) = '0' else '0';
  s1_axi_wready_l  <= m_axi_wready when uslsw(7) = '0' else '0';
  s1_axi_bid       <= s1_axi_m2s_awid & m_axi_bid;
  s1_axi_bresp     <= m_axi_bresp;
  s1_axi_bvalid_l  <= m_axi_bvalid when uslsw(7) = '0' else '0';

  s1_axi_arready   <= s1_axi_arready_l;
  s1_axi_rvalid    <= s1_axi_rvalid_l;
  s1_axi_awready   <= s1_axi_awready_l;
  s1_axi_wready    <= s1_axi_wready_l;
  s1_axi_bvalid    <= s1_axi_bvalid_l;

  -- s0_axi read-write requests
  s0_axi_pr: process(aclk)
    -- idle: waiting for axi master requests: when receiving write address and data valid (higher priority than read), perform the write, assert write address
    --       ready, write data ready and bvalid, go to w1, else, when receiving address read valid, perform the read, assert read address ready, read data valid
    --       and go to r1
    -- w1:   deassert write address ready and write data ready, wait for write response ready: when receiving it, deassert write response valid, go to idle
    -- r1:   deassert read address ready, wait for read response ready: when receiving it, deassert read data valid, go to idle
    type state_type is (idle, w1, r1);
    variable state: state_type;
    variable idx: natural range 0 to 2**9 - 1;
  begin
    if rising_edge(aclk) then
      if aresetn = '0' then
        s0_axi_arready <= '0';
        s0_axi_rdata   <= (others => '0');
        s0_axi_rresp   <= (others => '0');
        s0_axi_rvalid  <= '0';
        s0_axi_awready <= '0';
        s0_axi_wready  <= '0';
        s0_axi_bresp   <= (others => '0');
        s0_axi_bvalid  <= '0';
        state := idle;
      else
        -- s0_axi write and read
        case state is
          when idle =>
            if s0_axi_awvalid = '1' and s0_axi_wvalid = '1' then -- write address and data
              idx := to_integer(unsigned(s0_axi_awaddr(11 downto 4)));
              if idx = 1 then -- if r register
                s0_axi_bresp <= axi_resp_okay;
                for i in 0 to 7 loop
                  if s0_axi_wstrb(i) = '1' then
                    r(8 * i + 7 downto 8 * i) <= s0_axi_wdata(8 * i + 7 downto 8 * i);
                  end if;
                end loop;
              elsif idx < ndwords then -- if read-only status or axi_reg register
                s0_axi_bresp <= axi_resp_slverr;
              else
                s0_axi_bresp <= axi_resp_decerr;
              end if;
              s0_axi_awready <= '1';
              s0_axi_wready <= '1';
              s0_axi_bvalid <= '1';
              state := w1;
            elsif s0_axi_arvalid = '1' then
              if idx >= ndwords then -- if unmapped address
                s0_axi_rdata <= (others => '0');
                s0_axi_rresp <= axi_resp_decerr;
              else
                s0_axi_rresp <= axi_resp_okay;
                s0_axi_rdata <= regs(64 * idx + 63 downto 64 * idx);
              end if;
              s0_axi_arready <= '1';
              s0_axi_rvalid <= '1';
              state := r1;
            end if;
          when w1 =>
            s0_axi_awready <= '0';
            s0_axi_wready <= '0';
            if s0_axi_bready = '1' then
              s0_axi_bvalid <= '0';
              state := idle;
            end if;
          when r1 =>
            s0_axi_arready <= '0';
            if s0_axi_rready = '1' then
              s0_axi_rvalid <= '0';
              state := idle;
            end if;
        end case;
      end if;
    end if;
  end process s0_axi_pr;

end architecture rtl;
