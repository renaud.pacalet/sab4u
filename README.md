This repository and its sub-directories contain the VHDL source code, simulation and synthesis scripts for SAB4U, a simple design example for the Xilinx Zynq UltraScale+ core. It can be ported on any board based on Xilinx Zynq UltraScale+ cores but has been specifically designed for the Xilinx ZCU102 board.

All provided instructions are for a host computer running a GNU/Linux operating system and have been tested on a Debian 9 (stretch) distribution. Porting to other GNU/Linux distributions should be very easy. If you are working under Microsoft Windows or Apple Mac OS X, installing a virtualization framework and running a Debian OS in a virtual machine is probably the easiest path.

The version of the Xilinx tools that was used is 2017.3, update 1.

Please signal errors and send suggestions for improvements to [renaud.pacalet@telecom-paristech.fr][email].

# Table of content
* [License](#license)
* [Useful links](#useful-links)
* [Content](#content)
* [Description](#description)

# License

Copyright (C) Telecom ParisTech  
Copyright (C) Renaud Pacalet ([renaud.pacalet@telecom-paristech.fr][email])

Licensed under the CeCILL license, Version 2.1 of
2013-06-21 (the "License"). You should have
received a copy of the License. Else, you may
obtain a copy of the License at:

[http://www.cecill.info/licences/Licence\_CeCILL\_V2.1-en.txt][cecill]

# Useful links

* [Xilinx][xilinx]
* [ZCU102 board user guide][zcu102]

# Content

```
├── COPYING                       License (English version)
├── COPYING-FR                    Licence (version française)
├── COPYRIGHT                     Copyright notice
├── hdl                           VHDL source code
│   ├── sab4u_sim.vhd             Simulation environment for SAB4U
│   └── sab4u.vhd                 Top-level SAB4U
├── images                        Figures
│   ├── sab4u.fig                 SAB4U in Zynq UltraScale+
│   └── sab4u.png                 PNG export of sab4u.fig
├── Makefile                      Main makefile
├── README.md                     This file
└── scripts                       Scripts
    └── vvsyn.tcl                 Vivado TCL synthesis script
```

# Description

## Assumptions

* In the following we denote _Regular Address Space_ (`RAS`) the `[0x08_0000_0000..0x09_0000_0000[` range of the Global System Address Map (GSAM). For a complete view of the GSAM, please see figure 10.1 of the [Zynq Ultrascale+ Technical Reference Manual] by [Xilinx]. This documentation assumes that accesses to the `RAS` by the PS or by the PL fall in the 4GB DDR4 SODIMM module of the ZCU102 board. Note that the Zynq UltraScale+ system protection units can be configured to change this mapping, or to block accesses, making this assumption wrong. Note also that the DDR controller also has translation capabilities that can change this mapping.

## Functional description

**SAB4U** is a **S**imple **A**xi-to-axi **B**ridge **For** Zynq **U**ltraScale+ cores. Its VHDL synthesizable model is available in the `hdl` sub-directory. The following figure represents SAB4U mapped in the Programmable Logic (PL) of the Zynq UltraScale+ core:

![SAB4U on a Zynq-based board](images/sab4u.png)

SAB4U embeds internal registers and has a 8-bits input (`SW`), a 8-bits output (`LED`) and a one-bit command input (`BTN`). SAB4U is connected to the Processing System (PS) of the Zynq UltraScale+ core by its 3 AXI interfaces:

* `S0_AXI`: a slave lite AXI interface with 12 bits addresses (4kB) and 64 bits data, connected to the `M_AXI_HPM0_FPD` master AXI interface of the PS, and used to access the SAB4U internal registers. It is mapped to the `[0x00_a000_0000..0x00_a000_1000[` range of the GSAM, denoted _Control Address Space_ (`CAS`) in this documentation.
* `S1_AXI`: a slave AXI interface with 32 bits addresses (4GB) and 64 bits data, connected to the `M_AXI_HPM1_FPD` master AXI interface of the PS. It is mapped to the `[0x05_0000_0000..0x06_0000_0000[` range of the GSAM, denoted _Alternate Address Space_ (`AAS`) in this documentation.
* `M_AXI`: a master AXI interface with 36 bits addresses (64GB) and 64 bits data, connected to the `S_AXI_HP1_FPD` slave AXI interface of the PS. It is mapped to the `[0x00_0000_0000..0x10_0000_0000[` range of the GSAM, but as explained below only the `[0x08_0000_0000..0x09_0000_0000[` range is actually used.

The `S1_AXI` and `M_AXI` AXI interfaces are connected together by the SAB4U design to form a bridge. The 32 bits addresses of the AXI requests received on `S1_AXI` are extended to 36 bits and up-shifted by `0x8_0000_0000` (from `AAS` to `RAS`) before they are forwarded to `M_AXI`. Example: if the PS performs an access at address `0x05_1234_5678`, it will be routed to the `S1_AXI` slave AXI interface with the PL and only the 32 LSBs of the address will be retained, that is, `0x1234_5678`. SAB4U will extend this address to 36 bits (`0x0_1234_5678`) and up-shift it by `0x8_0000_0000` (`0x8_1234_5678`) before sending it to the `M_AXI` master AXI interface. With the above-mentionned assumptions, this access should next be routed to the DDR controller and, finally, to address `1234_5678` of the 4GB DDR4 SODIMM module of the ZCU102 board. It should thus be the same as accessing directly address `0x8_1234_5678` from the PS. From the PS perspective, ranges `[0x08_0000_0000..0x09_0000_0000[` (`RAS`) and `[0x05_0000_0000..0x06_0000_0000[` (`AAS`) of the GSAM are thus equivalent (except for caching and potentially for access control and other side effects).

This allows the PS to access the 4GB DDR4 SODIMM module through the PL (in the `AAS` range) instead of accessing it directly (in the `RAS` range) and gives the opportunity to implement in the PL a monitoring or processing of the PS accesses to the DDR. SABU4 only counts the requests on each of the 5 AXI channels but it should be reasonably easy to implement something more sophisticated.

The `SW` input (`SW(7)`) can be used to disable the `S1_AXI` to `M_AXI` bridge of SAB4U: when it is set, the bridge is disabled in both ways; requests and responses are not forwarded any more. If accesses to the `AAS` abort, please check the `SW(7)` input.

## Remarks

* The `S_AXI_HP1_FPD` salve AXI interface of the PS connected to the `M_AXI` master AXI interface of SAB4U is not cache-coherent. It is thus highly discouraged to access the same DDR locations using both `RAS` and `AAS`.
* Moreover, randomly modifying the content of DDR using the `AAS` can crash the system or lead to unexpected behaviours if the modified region is currently in use through `RAS` by the currently running software stack. This is a second very good reason to not access the same DDR locations using both `RAS` and `AAS`.

## Control address space

`CAS` is used to access the internal registers. Its mapping is:

| Address          | Mapping                   | Description                                  |
| :--------------- | :------------------------ | :------------------------------------------- |
| `0x00_a000_0000` | `STATUS(63..0)` (ro)      | 64 bits read-only status register            |
| `0x00_a000_0008` | `R(63..0)`      (rw)      | 64 bits general purpose read-write register  |
| `0x00_a000_0010` | `AXI_REG(63..0)` (ro)     | 256 bits read-only AXI debug register (LSBs) |
| `0x00_a000_0018` | `AXI_REG(127..64)` (ro)   | 256 bits read-only AXI debug register        |
| `0x00_a000_0020` | `AXI_REG(191..128)` (ro)  | 256 bits read-only AXI debug register        |
| `0x00_a000_0028` | `AXI_REG(255..192)` (ro)  | 256 bits read-only AXI debug register (MSBs) |
| `0x00_a000_0030` | Unmapped                  |                                              |
| ...              | ...                       |                                              |
| `0x00_a000_0ff8` | Unmapped                  |                                              |

Accesses to the unmapped region of `CAS` raise `DECERR` AXI errors. Write accesses to the read-only registers raise `SLVERR` AXI errors. The content of the registers can also be sent to the 8 bits `LED` output, one byte at a time. Which byte of which register drives the `LED` output is selected by the 6 LSBs of the `SW` input:

| `SW(5..0)` | `LED`               |
| :-------   | :-------------------|
| `000000`   | `STATUS(7..0)`      |
| `000001`   | `STATUS(15..8)`     |
| ...        | ...                 |
| `000111`   | `STATUS(63..56)`    |
| `001000`   | `R(7..0)`           |
| ...        | ...                 |
| `001111`   | `R(63..56)`         |
| `010000`   | `AXI_REG(7..0)`     |
| ...        | ...                 |
| `101111`   | `AXI_REG(255..248)` |

The `STATUS` register is subdivided in 8 one-byte fields:

| Register         | Field      | Description                                     |
| :--------------- | :--------- | :---------------------------------------------- |
| `STATUS(7..0)`   | `ERROR`    | AXI ID mismatch error indicators                |
| `STATUS(15..8)`  | `LIFE`     | Rotating bit (life monitor)                     |
| `STATUS(23..16)` | `CNT`      | Counter of `BTN` rising edges                   |
| `STATUS(31..24)` | `ARCNT`    | Counter of `S1_AXI` address-read transactions   |
| `STATUS(39..32)` | `RCNT`     | Counter of `S1_AXI` read-response transactions  |
| `STATUS(47..40)` | `AWCNT`    | Counter of `S1_AXI` address-write transactions  |
| `STATUS(55..48)` | `WCNT`     | Counter of `S1_AXI` data-write transactions     |
| `STATUS(63..56)` | `BCNT`     | Counter of `S1_AXI` write-response transactions |

Only the two LSBs of the `ERROR` field of the `STATUS` register are used. `ERROR(0)` is asserted high if the 10 MSBs of the 16 bits `ARID` field of the AXI read transactions received on the `S1_AXI` interface are not constant, which is not supported by the current version of SAB4U. Same for `ERROR(1)` with the address write AXI transactions. The 6 other bits of `ERROR` are wired to 0.

`CNT` is an 8-bits counter. It is initialized to zero after reset. Each time there is a rising edge on the `BTN` input, `CNT` is incremented (modulus 256). When `BTN` is high, the 8-bits `LED` output is driven by the 8 bits `SW` input (a way to check its current value). The `BTN` input is filtered by a debouncer-resynchronizer before being used to increment `CNT`.

The `R` register is a general purpose, 64-bits, read-write register.

The `AXI_REG` register (256 bits) is used to store the various fields of the AXI requests received on the `S1_AXI` interface. Each time a new address-read, address-write or data-write request is received, the corresponding fields of `AXI_REG` are updated. This register can be used for debugging. Its layout is the following (missing bits are wired to 0):

| byte(s) | bit(s) | AXI field | Updated upon  |
| :-------| :------| :-------- | :------------ |
| 31..30  | 15..0  | `ARID`    | address-read  |
| 29..28  | 15..0  | `AWID`    | address-write |
| 27..24  | 31..0  | `ARADDR`  | address-read  |
| 23..20  | 31..0  | `AWADDR`  | address-write |
|     19  |  7..0  | `ARLEN`   | address-read  |
|     18  |  7..0  | `AWLEN`   | address-write |
|     17  |  6..4  | `ARSIZE`  | address-read  |
|     17  |  2..0  | `AWSIZE`  | address-write |
|     16  |  7..6  | `ARBURST` | address-read  |
|     16  |  5..4  | `AWBURST` | address-write |
|     16  |     3  | `ARVALID` | address-read  |
|     16  |     2  | `AWVALID` | address-write |
|     16  |     1  | `WLAST`   | data-write    |
|     16  |     0  | `WVALID`  | data-write    |
|     15  |     7  | `ARLOCK`  | address-read  |
|     15  |     6  | `AWLOCK`  | address-write |
|     15  |  5..3  | `ARPROT`  | address-read  |
|     15  |  2..0  | `AWPROT`  | address-write |
|     14  |  7..4  | `ARCACHE` | address-read  |
|     14  |  3..0  | `AWCACHE` | address-write |
|     13  |  7..4  | `ARQOS`   | address-read  |
|     13  |  3..0  | `AWQOS`   | address-write |
| 12..11  | 15..0  | `ARUSER`  | address-read  |
| 10...9  | 15..0  | `AWUSER`  | address-write |
|  8...1  | 63..0  | `WDATA`   | data-write    |
|      0  |  7..0  | `WSTRB`   | data-write    |

## IO mapping

On the ZCU102 board, the 8-bits `LED` output, the 8-bits `SW` input and the `BTN` input of SAB4U are connected to the 8 GPIO LEDs, the 8 GPIO DIP switches and the centre push-button of the 5 buttons pad, respectively. They are marked with callouts 21, 22 and 23 on figure 2-1, page 12 of the [board's user guide][zcu102].

[email]: mailto://renaud.pacalet@telecom-paristech.fr
[cecill]: http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
[xilinx]: http://www.xilinx.com/
[zcu102]: https://www.xilinx.com/support/documentation/boards_and_kits/zcu102/ug1182-zcu102-eval-bd.pdf
[Zynq Ultrascale+ Technical Reference Manual]: https://www.xilinx.com/support/documentation/user_guides/ug1085-zynq-ultrascale-trm.pdf
