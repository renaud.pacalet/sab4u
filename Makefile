#
# Copyright (C) Telecom ParisTech
# Copyright (C) Renaud Pacalet (renaud.pacalet@telecom-paristech.fr)
# 
# This file must be used under the terms of the CeCILL. This source
# file is licensed as described in the file COPYING, which you should
# have received as part of this distribution. The terms are also
# available at:
# http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt
#

.NOTPARALLEL:

#############
# Variables #
#############

# General purpose
DEBUG	?= 1
SHELL	:= /bin/bash

ifeq ($(DEBUG),0)
OUTPUT	:= &> /dev/null
else ifeq ($(DEBUG),1)
OUTPUT	:= > /dev/null
else
OUTPUT	:=
endif

rootdir		:= $(realpath .)
BUILD		?= /tmp/sab4u.builds
HDLDIR		:= hdl
HDLSRCS		:= $(wildcard $(HDLDIR)/*.vhd)
SCRIPTS		:= scripts

# Mentor Graphics Modelsim
MSBUILD		?= $(BUILD)/ms
MSCONFIG	:= $(MSBUILD)/modelsim.ini
MSLIB		:= vlib
MSMAP		:= vmap
MSCOM		:= vcom
MSCOMFLAGS	:= -ignoredefaultbinding -nologo -quiet -2002
MSSIM		:= vsim
MSSIMFLAGS	:= -c -voptargs="+acc" -do 'run -all; quit'
MSTAGS		:= $(patsubst $(HDLDIR)/%.vhd,$(MSBUILD)/%.tag,$(HDLSRCS))

# Xilinx Vivado
VVMODE		?= batch
VIVADO		:= vivado
VVBUILD		?= $(BUILD)/vv
VVSCRIPT	:= $(SCRIPTS)/vvsyn.tcl
# Supported boards: zybo, zed, zc706
VVBOARD		?= zcu102
VIVADOFLAGS	:= -mode $(VVMODE) -notrace -source $(VVSCRIPT) -tempDir /tmp -journal $(VVBUILD)/vivado.jou -log $(VVBUILD)/vivado.log -tclargs $(rootdir) $(VVBUILD) $(VVBOARD)
VVIMPL		:= $(VVBUILD)/top.runs/impl_1
VVBIT		:= $(VVIMPL)/top_wrapper.bit

# Software Design Kits
XDTS			?= /opt/downloads/device-tree-xlnx
HSI			:= hsi
SYSDEF			:= $(VVIMPL)/top_wrapper.sysdef
DTSSCRIPT		:= $(SCRIPTS)/dts.tcl
DTSFLAGS		:= -mode batch -quiet -notrace -nojournal -nolog -tempDir /tmp
DTSBUILD		?= $(BUILD)/dts
DTSTOP			:= $(DTSBUILD)/system.dts
FSBLSCRIPT		:= $(SCRIPTS)/fsbl.tcl
FSBLFLAGS		:= -mode batch -quiet -notrace -nojournal -nolog -tempDir /tmp
FSBLBUILD		?= $(BUILD)/fsbl
FSBLTOP			:= $(FSBLBUILD)/main.c

# Misc
ARCHIVE		:= $(VVBOARD).tar.xz

# Messages
define HELP_message

make targets:
  help       print this message (default goal)
  ms-all     compile all VHDL source files with Modelsim ($(MSBUILD))
  ms-sim     simulate SAB4U with Modelsim
  ms-clean   delete all files and directories automatically created by Modelsim
  vv-all     synthesize design with Vivado ($(VVBUILD))
  vv-clean   delete all files and directories automatically created by Vivado
  dts        generate device tree sources ($(DTSBUILD))
  dts-clean  delete device tree sources
  fsbl       generate First Stage Boot Loader (FSBL) sources ($(FSBLBUILD))
  fsbl-clean delete FSBL sources
  doc        generate documentation images
  doc-clean  delete generated documentation images
  archive    package the ready-to-use $(ARCHIVE) archive
  clean      delete all automatically created files and directories

directories:
  hdl sources          ./$(HDLDIR)
  build                $(BUILD)
  Modelsim build       $(MSBUILD)
  Vivado build         $(VVBUILD)
  Device Tree Sources  $(DTSBUILD)
  FSBL sources         $(FSBLBUILD)

customizable make variables:
  DEBUG       debug level: 0=none, 1: some, 2: verbose ($(DEBUG))
  BUILD       main build directory ($(BUILD))
  MSBUILD     build directory for Mentor Graphics Modelsim ($(MSBUILD))
  VVMODE      Vivado running mode (gui, tcl or batch) ($(VVMODE))"
  VVBUILD     build directory for Xilinx Vivado ($(VVBUILD))
  VVBOARD     target board (zybo, zed or zc706) ($(VVBOARD))"
  XDTS        clone of Xilinx device trees git repository ($(XDTS))
  DTSBUILD    build directory for the device tree ($(DTSBUILD))
  FSBLBUILD   build directory for the First Stage Boot Loader ($(FSBLBUILD))
endef
export HELP_message

################
# Make targets #
################

# Help
help::
	@echo "$$HELP_message"

# Mentor Graphics Modelsim
ms-all: $(MSTAGS)

$(MSTAGS): $(MSBUILD)/%.tag: $(HDLDIR)/%.vhd
	@echo '[MSCOM] $<' && \
	cd $(MSBUILD) && \
	$(MSCOM) $(MSCOMFLAGS) $(rootdir)/$< && \
	touch $@

$(MSTAGS): $(MSCONFIG)

$(MSCONFIG):
	@echo '[MKDIR] $(MSBUILD)' && \
	mkdir -p $(MSBUILD) && \
	cd $(MSBUILD) && \
	$(MSLIB) .work $(OUTPUT) && \
	$(MSMAP) work .work $(OUTPUT)

$(MSBUILD)/sab4u_sim.tag: $(MSBUILD)/sab4u.tag

.PHONY: sim

ms-sim: $(MSBUILD)/sab4u_sim.tag
	@echo '[MSSIM] $<' && \
	cd $(MSBUILD) && \
	$(MSSIM) $(MSSIMFLAGS) work.sab4u_sim

ms-clean:
	@echo '[RM] $(MSBUILD)' && \
	rm -rf $(MSBUILD)

# Xilinx Vivado
vv-all: $(VVBIT)

$(VVBIT): $(HDLSRCS) $(VVSCRIPT)
	@echo '[VIVADO] $(VVSCRIPT)' && \
	mkdir -p $(VVBUILD) && \
	$(VIVADO) $(VIVADOFLAGS)

$(SYSDEF):
	@$(MAKE) vv-all

vv-clean:
	@echo '[RM] $(VVBUILD)' && \
	rm -rf $(VVBUILD)

# Device tree
dts: $(DTSTOP)

$(DTSTOP): $(SYSDEF) $(DTSSCRIPT)
	@if [ ! -d $(XDTS) ]; then \
		echo 'Xilinx device tree source directory $(XDTS) not found.' && \
		exit -1; \
	fi && \
	echo '[HSI] $< --> $(DTSBUILD)' && \
	$(HSI) $(DTSFLAGS) -source $(DTSSCRIPT) -tclargs $(SYSDEF) $(XDTS) $(DTSBUILD) $(OUTPUT)

dts-clean:
	@echo '[RM] $(DTSBUILD)' && \
	rm -rf $(DTSBUILD)

# First Stage Boot Loader (FSBL)
fsbl: $(FSBLTOP)

$(FSBLTOP): $(SYSDEF) $(FSBLSCRIPT)
	@echo '[HSI] $< --> $(FSBLBUILD)' && \
	$(HSI) $(FSBLFLAGS) -source $(FSBLSCRIPT) -tclargs $(SYSDEF) $(FSBLBUILD) $(OUTPUT)

fsbl-clean:
	@echo '[RM] $(FSBLBUILD)' && \
	rm -rf $(FSBLBUILD)

# Documentation
FIG2DEV		:= fig2dev
FIG2DEVFLAGS	:= -Lpng -m2.0 -S4
FIGS		:= $(wildcard images/*.fig)
PNGS		:= $(patsubst %.fig,%.png,$(FIGS))

doc: $(PNGS)

$(PNGS): %.png: %.fig
	$(FIG2DEV) $(FIG2DEVFLAGS) $< $@

doc-clean:
	@echo '[RM] $(PNGS)' && \
	rm -rf $(PNGS)

.PHONY: archive

archive: $(ARCHIVE)

$(ARCHIVE): $(VVBIT) $(SYSDEF) README.md $(VVSCRIPT) $(HDLSRCS)
	@d=$$(mktemp -d) && \
	mkdir -p $$d/$(VVBOARD) && \
	cp $^ $$d/$(VVBOARD) && \
	tar -C $$d -Jcvf $@ $(VVBOARD) && \
	echo "Archive created in $@"

# Full clean
clean: ms-clean vv-clean dts-clean fsbl-clean
