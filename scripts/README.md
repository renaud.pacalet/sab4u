# General set-up

```bash
$ export CROSS_COMPILE=aarch64-linux-gnu-
```

# Build FSBL with hsi

```bash
$ unset MAKEFLAGS
$ hsi
hsi% set hwdsgn [open_hw_design /tmp/sab4u.builds/vv/top.sdk/top_wrapper.hdf]
hsi% generate_app -hw $hwdsgn -os standalone -proc psu_cortexa53_0 -app zynqmp_fsbl -sw fsbl -dir fsbl
hsi% quit
$ make -C pmu -j1 CFLAGS='-Os -flto -ffat-lto-objects'
```

# Build PMU with hsi

```bash
$ unset MAKEFLAGS
$ hsi
hsi% set hwdsgn [open_hw_design /tmp/sab4u.builds/vv/top.sdk/top_wrapper.hdf]
hsi% generate_app -hw $hwdsgn -os standalone -proc psu_pmu_0 -app zynqmp_pmufw -sw pmufw -dir pmu
hsi% quit
$ make -C pmu -j1 CFLAGS='-Os -flto -ffat-lto-objects'
```

# Build Arm Trusted Firmware (ATF)

```bash
$ cd /opt/arm-trusted-firmware
$ make PLAT=zynqmp RESET_TO_BL31=1
```

# Build U-Boot

```bash
$ cd /opt/u-boot-xlnx
$ make O=/opt/builds/zcu102/u-boot xilinx_zynqmp_zcu102_rev1_0_config
$ cd /opt/builds/zcu102/u-boot
$ make -j
```

# Build device tree with hsi

```bash
$ hsi
hsi% set hwdsgn [open_hw_design /tmp/sab4u.builds/vv/top.sdk/top_wrapper.hdf]
hsi% set_repo_path /opt/device-tree-xlnx
hsi% create_sw_design device-tree -os device_tree -proc psu_cortexa53_0
hsi% set_property CONFIG.periph_type_overrides "{BOARD zcu102-rev1.0}" [get_os]
hsi% generate_target -dir dts
hsi% quit
$ sed -i 's/ 0x000500000000 0x100000000/>, <0x5 0x0 0x1 0x0/' dts/pl.dtsi
$ /opt/bin/dtc -I dts -O dtb -o devicetree.dtb dts/system-top.dts
```

# Build Linux kernel

```bash
$ cd /opt/linux-xlnx
$ make O=/opt/builds/zcu102/linux ARCH=arm64 xilinx_zynqmp_defconfig
$ cd /opt/builds/zcu102/linux
$ make ARCH=arm64 menuconfig
$ make -j ARCH=arm64
```

