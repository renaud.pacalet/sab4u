set hwdsgn [open_hw_design /tmp/sab4u.builds/vv/top.sdk/top_wrapper.hdf]
set_repo_path /opt/device-tree-xlnx
create_sw_design device-tree -os device_tree -proc psu_cortexa53_0
set_property CONFIG.periph_type_overrides "{BOARD zcu102-rev1.0}" [get_os]
generate_target -dir dts
quit
