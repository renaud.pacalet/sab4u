#
# Copyright (C) Telecom ParisTech
# Copyright (C) Renaud Pacalet (renaud.pacalet@telecom-paristech.fr)
# 
# This file must be used under the terms of the CeCILL. This source
# file is licensed as described in the file COPYING, which you should
# have received as part of this distribution. The terms are also
# available at:
# http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt
#

proc usage {} {
	puts "usage: vivado -mode batch -source <script> -tclargs <rootdir> <builddir> \[<board>\]"
	puts "  <rootdir>:  absolute path of sab4u root directory"
	puts "  <builddir>: absolute path of build directory"
	puts "  <board>:    target board (zcu102, default zcu102)"
	exit -1
}

if { $argc != 3 } {
	usage
}

set rootdir [lindex $argv 0]
set builddir [lindex $argv 1]
set board [lindex $argv 2]
if { [ string equal -nocase "$board" "zcu102" ] } { 
	set board [get_board_parts xilinx.com:zcu102:*]
	set part "xczu9eg-ffvb1156-2-i"
	set psname "*xilinx.com:ip:zynq_ultra_ps_e:*"
	set bdrule "xilinx.com:bd_rule:zynq_ultra_ps_e"
	set frequency 200
	array set ios {
		"sw[0]"         { "AN14" "LVCMOS33" }
		"sw[1]"         { "AP14" "LVCMOS33" }
		"sw[2]"         { "AM14" "LVCMOS33" }
		"sw[3]"         { "AN13" "LVCMOS33" }
		"sw[4]"         { "AN12" "LVCMOS33" }
		"sw[5]"         { "AP12" "LVCMOS33" }
		"sw[6]"         { "AL13" "LVCMOS33" }
		"sw[7]"         { "AK13" "LVCMOS33" }
		"led[0]"        { "AG14" "LVCMOS33" }
		"led[1]"        { "AF13" "LVCMOS33" }
		"led[2]"        { "AE13" "LVCMOS33" }
		"led[3]"        { "AJ14" "LVCMOS33" }
		"led[4]"        { "AJ15" "LVCMOS33" }
		"led[5]"        { "AH13" "LVCMOS33" }
		"led[6]"        { "AH14" "LVCMOS33" }
		"led[7]"        { "AL12" "LVCMOS33" }
		"btn"           { "AG13" "LVCMOS33" }
	}
} else {
	usage
}

puts "*********************************************"
puts "Summary of build parameters"
puts "*********************************************"
puts "Board: $board"
puts "Part: $part"
puts "Root directory: $rootdir"
puts "Build directory: $builddir"
puts "*********************************************"

cd $builddir

set ip sab4u
set lib sab4u_lib
set vendor www.telecom-paristech.fr

###################
# Create SAB4Z IP #
###################
create_project -part $part -force $ip $ip
add_files $rootdir/hdl/$ip.vhd
ipx::package_project -root_dir $ip -vendor $vendor -library $lib -generated_files -import_files -force
close_project
puts "$ip IP created"

############################
## Create top level design #
############################
set top top
create_project -part $part -force $top .
set_property board_part $board [current_project]
set_property ip_repo_paths [list ./$ip ] [current_fileset]
update_ip_catalog
create_bd_design "$top"
set u0 [create_bd_cell -type ip -vlnv [get_ipdefs *www.telecom-paristech.fr:$lib:$ip:*] $ip]
set_property -dict [list CONFIG.FREQUENCY [list [expr $frequency * 1000000]]] $u0
set ps [create_bd_cell -type ip -vlnv [get_ipdefs $psname] ps]
apply_bd_automation -rule $bdrule -config {apply_board_preset "1" } $ps
set_property -dict [list CONFIG.PSU__HIGH_ADDRESS__ENABLE {1}] $ps
set_property -dict [list CONFIG.PSU__CRL_APB__PL0_REF_CTRL__FREQMHZ [list $frequency]] $ps
set_property -dict [list CONFIG.PSU__MAXIGP0__DATA_WIDTH {64}] $ps
set_property -dict [list CONFIG.PSU__MAXIGP1__DATA_WIDTH {64}] $ps
set_property -dict [list CONFIG.PSU__USE__S_AXI_GP3 {1}] $ps
set_property -dict [list CONFIG.PSU__SAXIGP3__DATA_WIDTH {64}] $ps

# Interconnections
# Primary IOs
create_bd_port -dir O -from 7 -to 0 led
connect_bd_net [get_bd_pins /$u0/led] [get_bd_ports led]
create_bd_port -dir I -from 7 -to 0 sw
connect_bd_net [get_bd_pins /$u0/sw] [get_bd_ports sw]
create_bd_port -dir I btn
connect_bd_net [get_bd_pins /$u0/btn] [get_bd_ports btn]
# ps - sab4u
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/ps/M_AXI_HPM0_FPD" intc_ip "Auto" Clk_xbar "/ps/pl_clk0" Clk_master "/ps/pl_clk0" Clk_slave "/ps/pl_clk0" } [get_bd_intf_pins $ip/s0_axi]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/ps/M_AXI_HPM1_FPD" intc_ip "Auto" Clk_xbar "/ps/pl_clk0" Clk_master "/ps/pl_clk0" Clk_slave "/ps/pl_clk0" } [get_bd_intf_pins $ip/s1_axi]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/sab4u/m_axi" intc_ip "Auto" Clk_xbar "/ps/pl_clk0" Clk_master "/ps/pl_clk0" Clk_slave "/ps/pl_clk0" }  [get_bd_intf_pins ps/S_AXI_HP1_FPD]

# Addresses ranges
set_property range 4K [get_bd_addr_segs {ps/Data/SEG_sab4u_reg0}]
set_property offset 0x00a0000000 [get_bd_addr_segs {ps/Data/SEG_sab4u_reg0}]
set_property range 4G [get_bd_addr_segs {ps/Data/SEG_sab4u_reg01}]
set_property offset 0x0500000000 [get_bd_addr_segs {ps/Data/SEG_sab4u_reg01}]
#set_property range 4G [get_bd_addr_segs {sab4u/m_axi/SEG_ps_HP1_DDR_HIGH}]
#set_property offset 0x0800000000 [get_bd_addr_segs {sab4u/m_axi/SEG_ps_HP1_DDR_HIGH}]

# Synthesis flow
validate_bd_design
set files [get_files *$top.bd]
generate_target all $files
add_files -norecurse -force [make_wrapper -files $files -top]
save_bd_design
set synth [get_runs synth*]
set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY none $synth
set_property strategy Flow_RuntimeOptimized $synth
launch_runs $synth
wait_on_run $synth
open_run $synth

# IOs
foreach io [ array names ios ] {
	set pin [ lindex $ios($io) 0 ]
	set std [ lindex $ios($io) 1 ]
	set_property package_pin $pin [get_ports $io]
	set_property iostandard $std [get_ports [list $io]]
}

# Timing constraints
set clock [get_clocks]
set_false_path -from $clock -to [get_ports {led[*]}]
set_false_path -from [get_ports {btn sw[*]}] -to $clock

# Implementation
save_constraints
set impl [get_runs impl*]
set_property strategy Flow_RuntimeOptimized $impl
reset_run $impl
set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true $impl
launch_runs -to_step write_bitstream $impl
wait_on_run $impl

# Messages
set rundir ${builddir}/$top.runs/$impl
puts ""
puts "*********************************************"
puts "\[VIVADO\]: done"
puts "*********************************************"
puts "Summary of build parameters"
puts "*********************************************"
puts "Board: $board"
puts "Part: $part"
puts "Root directory: $rootdir"
puts "Build directory: $builddir"
puts "*********************************************"
puts "  bitstream in $rundir/${top}_wrapper.bit"
puts "  resource utilization report in $rundir/${top}_wrapper_utilization_placed.rpt"
puts "  timing report in $rundir/${top}_wrapper_timing_summary_routed.rpt"
puts "*********************************************"
